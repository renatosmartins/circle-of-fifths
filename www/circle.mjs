import Segment from "./segment.mjs"
import keys from "./keys.mjs"

const keypressToCentreKey = {
  ArrowLeft: -1,
  ArrowRight: 1,
}

class Circle extends HTMLElement {
  constructor() {
    super()
    this.centreKey = 0
    this.segments = keys.map((key, index) => new Segment(index, key))
  }

  connectedCallback() {
    this.render()
    this.setCentreKey(0)
    this.setupListeners()
  }

  render() {
    this.classList.add("circle")
    this.append(...this.segments.map((segment) => segment))
  }

  setupListeners() {
    document.addEventListener("keydown", (e) => {
      if (!Object.keys(keypressToCentreKey).includes(e.key)) {
        return
      }
      e.preventDefault()
      this.setCentreKey(
        (keys.length + this.centreKey + keypressToCentreKey[e.key]) %
          keys.length
      )
    })

    this.addEventListener("centre-key", ({ detail }) => {
      this.setCentreKey(detail)
    })
  }

  setCentreKey(value) {
    this.segments[this.centreKey].setCentre(false)
    this.centreKey = value
    this.selectedKeys = [
      this.centreKey,
      (keys.length + this.centreKey - 1) % keys.length,
      (this.centreKey + 1) % keys.length,
    ]
    this.segments.forEach((s) =>
      s.setSelected(this.selectedKeys.includes(s.index))
    )
    this.segments[this.centreKey].setCentre(true)
    this.style = `--centre-key: ${this.centreKey}`
  }
}

customElements.define("x-circle", Circle)

export default Circle
