export default [
  {
    major: "C",
    minor: "a",
    diminished: "b",
  },
  {
    major: "G",
    minor: "e",
    diminished: "f♯",
  },
  {
    major: "D",
    minor: "b",
    diminished: "c♯",
  },
  {
    major: "A",
    minor: "f♯",
    diminished: "g♯",
  },
  {
    major: "E",
    minor: "c♯",
    diminished: "d♯",
  },
  {
    major: "B",
    minor: "g♯",
    diminished: "a♯",
  },
  {
    major: "F♯",
    minor: "d♯",
    diminished: "e♯",
  },
  {
    major: "D♭",
    minor: "b♭",
    diminished: "c",
  },
  {
    major: "A♭",
    minor: "f",
    diminished: "g",
  },
  {
    major: "E♭",
    minor: "c",
    diminished: "d",
  },
  {
    major: "B♭",
    minor: "g",
    diminished: "a",
  },
  {
    major: "F",
    minor: "d",
    diminished: "e",
  },
]
