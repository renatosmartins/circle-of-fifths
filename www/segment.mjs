import keys from "./keys.mjs"

class Segment extends HTMLElement {
  constructor(index, data) {
    super()
    this.index = index
    this.data = data
    this.selected = false
    this.centre = false
  }

  connectedCallback() {
    if (this.index === undefined) {
      this.index = Number.parseInt(this.getAttribute("index") || 0)
      this.data = keys[this.index % keys.length]
    }
    this.render()
    this.setupListeners()
  }

  render() {
    this.classList.add("segment")
    this.style = `--index: ${this.index}`
    const keys = document.createElement("div")
    keys.classList.add("keys")
    const major = document.createElement("div")
    major.setAttribute("aria-label", `${this.data.major} major`)
    major.classList.add("key", "major")
    major.textContent = this.data.major
    const minor = document.createElement("div")
    minor.setAttribute("aria-label", `${this.data.minor} minor`)
    minor.classList.add("key", "minor")
    minor.textContent = this.data.minor
    const diminished = document.createElement("div")
    diminished.setAttribute("aria-label", `${this.data.diminished} diminished`)
    diminished.classList.add("key", "diminished")
    diminished.textContent = this.data.diminished
    keys.append(major, minor, diminished)
    this.append(keys)
  }

  setupListeners() {
    this.addEventListener("click", () => {
      this.dispatchEvent(new CustomEvent("centre-key", { detail: this.index, bubbles: true }))
    })
  }

  setSelected(toggle) {
    if (toggle === this.selected) {
      return
    }
    this.selected = toggle
    this.classList.toggle("segment--selected", toggle)
  }

  setCentre(toggle) {
    this.centre = toggle
    this.classList.toggle("segment--centre", toggle)
  }
}

customElements.define("x-segment", Segment)

export default Segment
