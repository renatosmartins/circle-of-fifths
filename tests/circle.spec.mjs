import "@testing-library/jest-dom"
import { fireEvent } from "@testing-library/dom"
import Circle from "../www/circle.mjs"
import { describe, expect, it } from "@jest/globals"

describe(Circle, () => {
  const setup = () => {
    const circle = new Circle()
    document.body.append(circle)
    const segments = circle.querySelectorAll(".segment")
    return { circle, segments }
  }

  beforeEach(() => {
    document.body.innerHTML = ""
  })

  it("renders", () => {
    const { circle } = setup()
    expect(circle).toMatchSnapshot()
  })

  it("renders with <x-circle>", () => {
    document.body.innerHTML = `<x-circle />`
    expect(document.querySelectorAll(".circle")).toHaveLength(1)
    expect(document.querySelectorAll(".segment")).toHaveLength(12)
  })

  it("sets a centre key", () => {
    const { circle, segments } = setup()
    expect(circle.centreKey).toBe(0)
    expect(segments[11].className).toBe("segment segment--selected")
    expect(segments[0].className).toBe("segment segment--selected segment--centre")
    expect(segments[1].className).toBe("segment segment--selected")

    circle.setCentreKey(5)
    expect(circle.centreKey).toBe(5)
    expect(segments[11].className).toBe("segment")
    expect(segments[0].className).toBe("segment")
    expect(segments[1].className).toBe("segment")
    expect(segments[4].className).toBe("segment segment--selected")
    expect(segments[5].className).toBe("segment segment--selected segment--centre")
    expect(segments[6].className).toBe("segment segment--selected")
  })

  it("listens to event to set a centre key", () => {
    const { segments } = setup()
    fireEvent.click(segments[9])
    expect(segments[8].className).toBe("segment segment--selected")
    expect(segments[9].className).toBe("segment segment--selected segment--centre")
    expect(segments[10].className).toBe("segment segment--selected")
  })

  it("turns circle left", () => {
    const { circle } = setup()
    expect(circle.centreKey).toBe(0)
    fireEvent.keyDown(circle, { key: "ArrowLeft" })
    expect(circle.centreKey).toBe(11)
  })

  it("turns circle right", () => {
    const { circle } = setup()
    expect(circle.centreKey).toBe(0)
    fireEvent.keyDown(circle, { key: "ArrowRight" })
    expect(circle.centreKey).toBe(1)
  })

  it("ignores other keyboard events", () => {
    const { circle } = setup()
    expect(circle.centreKey).toBe(0)
    fireEvent.keyDown(circle, { key: "B" })
    expect(circle.centreKey).toBe(0)
  })
})
