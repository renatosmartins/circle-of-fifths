import "@testing-library/jest-dom"
import { fireEvent, queryByLabelText } from "@testing-library/dom"
import Segment from "../www/segment.mjs"
import { describe, expect, it, jest } from "@jest/globals"

describe(Segment, () => {
  const setup = (
    index = 0,
    data = { major: "C", minor: "a", diminished: "b" }
  ) => {
    const segment = new Segment(index, data)
    document.body.append(segment)
    return { segment }
  }

  beforeEach(() => {
    document.body.innerHTML = ""
  })

  it("renders", () => {
    const { segment } = setup()
    expect(segment).toMatchSnapshot()
  })

  it("renders with <x-segment> and index attribute", () => {
    document.body.innerHTML = `<x-segment index="4" />`
    expect(document.body.firstChild.index).toBe(4)
  })

  it("renders with <x-segment> and no index attribute", () => {
    document.body.innerHTML = `<x-segment />`
    expect(document.body.firstChild.index).toBe(0)
  })

  it("renders a major, minor and diminished keys", () => {
    const { segment } = setup()
    expect(queryByLabelText(segment, "C major")).toHaveTextContent("C")
    expect(queryByLabelText(segment, "a minor")).toHaveTextContent("a")
    expect(queryByLabelText(segment, "b diminished")).toHaveTextContent("b")
  })

  it("emits an event with its index when clicked", () => {
    const { segment } = setup(2)
    const callback = jest.fn()
    segment.addEventListener("centre-key", ({ detail }) => callback(detail))
    fireEvent.click(segment)
    expect(callback).toHaveBeenCalledWith(2)
  })

  it("renders as selected", () => {
    const { segment } = setup()
    expect(segment.className).toBe("segment")
    segment.setSelected(true)
    expect(segment.className).toBe("segment segment--selected")
  })

  it("renders as centred", () => {
    const { segment } = setup()
    expect(segment.classList.value).toBe("segment")
    segment.setCentre(true)
    expect(segment.classList.value).toBe("segment segment--centre")
  })
})
