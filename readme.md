# circle of fifths

[![pipeline status](https://gitlab.com/renatosmartins/circle-of-fifths/badges/main/pipeline.svg)](https://gitlab.com/renatosmartins/circle-of-fifths/-/commits/main) [![coverage report](https://gitlab.com/renatosmartins/circle-of-fifths/badges/main/coverage.svg)](https://gitlab.com/renatosmartins/circle-of-fifths/-/commits/main)

an interactive [circle of fifths](https://en.wikipedia.org/wiki/Circle_of_fifths)
